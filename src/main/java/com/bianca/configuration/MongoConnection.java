package com.bianca.configuration;

import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoConnection {

    private static MongoDatabase mongoDB;

    public static MongoDatabase getInstance(){
        if(mongoDB == null){
            MongoClient mongo;
            try {
                mongo = new MongoClient("localhost", 27017);
                mongoDB = mongo.getDatabase("demo");
                System.out.println("Connection successful \n");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return mongoDB;
    }

}
